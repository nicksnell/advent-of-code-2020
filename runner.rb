require 'optparse'

@options = {}

OptionParser.new do |parser|
  parser.on "--part1" do
    @options[:part1] = true
  end
  parser.on "--part2" do
    @options[:part2] = true
  end
  parser.on "--test" do
    @options[:test] = true
  end
  parser.on "--test2" do
    @options[:test2] = true
  end
end.parse!

if @options[:test]
  DATA_FILE = "data/#{$PROGRAM_NAME.sub('rb', 'test.txt')}"
elsif @options[:test2]
  DATA_FILE = "data/#{$PROGRAM_NAME.sub('rb', 'test2.txt')}"
else
  DATA_FILE = "data/#{$PROGRAM_NAME.sub('rb', 'txt')}"
end

if @options[:part1]
  part1
elsif @options[:part2]
  part2
else
  puts "Select a part to run!"
end
