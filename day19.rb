#!/usr/bin/env ruby

def expand(i, rules)
  if rules[i] == "a" or rules[i] == "b"
    return rules[i]
  end

  parts = rules[i].split(" | ")

  parts.each_with_index do |p, i|
    parts[i] = "(#{p.split.map { |n| expand(n.to_i, rules) }.join})"
  end

  return "(#{parts.join('|')})"
end

def part1
  rules = {}
  rule_txt, msgs = File.read(DATA_FILE).split("\n\n")

  rule_txt.split("\n").each do |r|
    m = r.match(/^(?<num>\d+): (?<rule>.+?)$/)
    rules[m['num'].to_i] = m['rule'].gsub('"', '')
  end

  valid = []
  matcher = "^#{expand(0, rules)}$"

  msgs.split("\n").each do |msg|
    if msg.match(matcher)
      valid << msg
    end
  end

  p valid.size
end

def part2

end

require './runner'
