#!/usr/bin/env ruby

def reduce(exp)
  while exp.include? "+" or exp.include? "*"
    r = exp.match(/\d+ [\+|\*] \d+/)
    exp = exp.sub(r[0], "#{eval(r[0])}")
  end
  exp
end

def reduce_precedence(exp)
  while exp.include? "+"
    r = exp.match(/\d+ \+ \d+/)
    exp = exp.sub(r[0], "#{eval(r[0])}")
  end
  while exp.include? "*"
    r = exp.match(/\d+ \* \d+/)
    exp = exp.sub(r[0], "#{eval(r[0])}")
  end
  exp
end

def part1
  values = []
  exps = File.readlines(DATA_FILE)
  exps.collect(&:strip).each do |exp|
    while exp.include? "("
      m = exp.match(/\(([\d\+\* ]+)\)/)
      exp = exp.sub(m[0], "#{reduce(m[1])}")
    end
    values << reduce(exp)
  end
  p values.map(&:to_i).reduce(:+)
end

def part2
  values = []
  exps = File.readlines(DATA_FILE)
  exps.collect(&:strip).each do |exp|
    while exp.include? "("
      m = exp.match(/\(([\d\+\* ]+)\)/)
      exp = exp.sub(m[0], "#{reduce_precedence(m[1])}")
    end
    values << reduce_precedence(exp)
  end
  p values.map(&:to_i).reduce(:+)
end

require './runner'
