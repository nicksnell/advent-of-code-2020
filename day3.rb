#!/usr/bin/env ruby

def get_slope
  matrix = {}

  lines = []
  File.open('data/day3.txt', 'r').each do |line|
    lines << line.strip!
  end

  lines.each_with_index do |line, row|
    line.split("").each.with_index do |char, col|
      matrix[[row, col]] = char
    end
  end

  return {
    :matrix => matrix,
    :width => lines[0].split("").count,
    :height => lines.count,
    :lines => lines
  }
end

def travel(slope, down, right)
  position = [0, 0]
  trees = 0

  until position[0] > slope[:height] do
    position = [position[0] + down, position[1] + right]

    if position[1] >= slope[:width]
      position[1] = position[1] - slope[:width]
    end

    if slope[:matrix][position] == '#'
      trees += 1
    end
  end

  return trees
end

def part1
  slope = get_slope
  puts travel(slope, 1, 3)
end

def part2
  slope = get_slope
  trees = [
    travel(slope, 1, 1),
    travel(slope, 1, 3),
    travel(slope, 1, 5),
    travel(slope, 1, 7),
    travel(slope, 2, 1)
  ]
  puts trees.reduce(:*)
end

require './runner'
