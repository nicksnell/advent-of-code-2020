#!/usr/bin/env ruby

def play(spoken, limit)
  last = spoken.length - 1

  until spoken.length > limit
    l = spoken[last]

    if spoken.count(l) <= 1
      spoken << 0
    elsif spoken.include? l
      a = spoken.rindex(l)
      b = spoken[0..a-1].rindex(l)
      spoken << a - b
    end

    last += 1
  end

  spoken
end

def part1
  spoken = []
  spoken += File.read(DATA_FILE).strip.split(',').map(&:to_i)
  result = play(spoken, 2020)
  p result
  p result[2019]
end

def part2
  spoken = []
  spoken += File.read(DATA_FILE).strip.split(',').map(&:to_i)
  result = play(spoken, 2020)

  seq = result[0...2]
  p result[3..-1].each_cons(seq.size).include? seq
  p '123'
end

require './runner'
