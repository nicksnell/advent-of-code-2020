#!/usr/bin/env ruby

def get_records
  File.read('data/day4.txt').gsub(/(?<=\w)\n/, ' ').split("\n")
end

def part1
  valid = 0
  required = ['byr','iyr','eyr','hgt','hcl','ecl','pid']

  get_records.each do |record|
    attribs = record.strip!.split(' ').map { |a| a.split(':') }.to_h
    if required.all? { |k| attribs.key? k }
      valid += 1
    end
  end

  puts valid
end

def part2
  valid = 0
  required = ['byr','iyr','eyr','hgt','hcl','ecl','pid']

  get_records.each do |record|
    attribs = record.strip!.split(' ').map { |a| a.strip.split(':') }.to_h
    if required.all? { |k| attribs.key? k }
      checks = [
        check_byr(attribs['byr']),
        check_iyr(attribs['iyr']),
        check_eyr(attribs['eyr']),
        check_hgt(attribs['hgt']),
        check_hcl(attribs['hcl']),
        check_ecl(attribs['ecl']),
        check_pid(attribs['pid']),
      ]

      if checks.all?
        valid += 1
      end
    end
  end

  puts valid
end

def check_byr(value)
  # byr (Birth Year) - four digits; at least 1920 and at most 2002.
  v = value.to_i
  v >= 1920 && v <= 2002
end

def check_iyr(value)
  # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
  v = value.to_i
  v >= 2010 && v <= 2020
end

def check_eyr(value)
  # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
  v = value.to_i
  v >= 2020 && v <= 2030
end

def check_hgt(value)
  # hgt (Height) - a number followed by either cm or in:
  # If cm, the number must be at least 150 and at most 193.
  # If in, the number must be at least 59 and at most 76.
  r = value.match(/(?<hgt>\d+)(?<unit>cm|in)/)

  if not r
    return false
  end

  hgt = r[:hgt].to_i

  if r[:unit] == 'cm' and hgt >= 150 and hgt <= 193
    return true
  end

  if r[:unit] == 'in' and hgt >= 59 and hgt <= 76
    return true
  end

  return false
end

def check_hcl(value)
  # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
  /^#[a-f0-9]{6}$/.match?(value)
end

def check_ecl(value)
  # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
  ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].include? value
end

def check_pid(value)
  # pid (Passport ID) - a nine-digit number, including leading zeroes.
  /^\d{9}$/.match?(value)
end

require './runner'
