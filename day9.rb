#!/usr/bin/env ruby

def get_nums
  nums = []
  File.readlines('data/day9.txt').each do |line|
    nums << line.strip.to_i
  end
  nums
end

def get_weak(nums, pre = 25)
  nums[pre..-1].each_with_index do |num, index|
    match = false
    nums[index..(index + pre - 1)].permutation(2).each do |perm|
      if perm.sum == num
        match = true
      end
    end
    unless match
      return num
    end
  end
end

def get_trail(nums, weak)
  nums.each_with_index do |num, i|
    trail = [num]
    nums[(i + 1)..-1].each do |nxt_num|
      if nxt_num > weak
        break
      end

      trail << nxt_num

      if trail.sum == weak
        return trail
      elsif trail.sum > weak
        break
      end
    end
  end
  return []
end

def part1
  nums = get_nums
  puts get_weak(nums)
end

def part2
  nums = get_nums
  weak = get_weak(nums)
  trail = get_trail(nums, weak).sort
  sum = trail[0] + trail[-1]
  puts sum
end

require './runner'
