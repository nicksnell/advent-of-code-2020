#!/usr/bin/env ruby

def mask_value(value, mask)
  value = value.split("")
  mask.each_with_index do |mv, i|
    unless mv == 'X'
      value[i] = mv
    end
  end
  value.join.to_i(2)
end

def mask_address(addr, mask)
  addresses = []

  addr = addr.split("")
  mask.each_with_index do |mv, i|
    if mv == "1" or mv == "X"
      addr[i] = mv
    end
  end

  addr = addr.join("")

  # Upper & lower variations
  addresses << addr.gsub("X", "0").to_i(2)
  addresses << addr.gsub("X", "1").to_i(2)

  if addr.count('X') > 1
    xs = addr.enum_for(:scan, /X/).map { Regexp.last_match.begin(0) }
    (1...addr.count('X')).each do |n|
      variations = ('1' * n).rjust(addr.count('X'), '0').split("").permutation
      variations.each do |var|
        v = addr.dup
        xs.each_with_index do |k, i|
          v[k] = var[i]
        end
        addresses << v.to_i(2)
      end
    end
  end

  addresses.uniq
end

def part1
  mem = {}
  mask = ''

  File.readlines(DATA_FILE).each do |line|
    if line.start_with? 'mask'
      mask = line.chomp.sub('mask = ', '').split("")
    else
      r = line.match(/^mem\[(?<addr>\d+)\] = (?<val>\d+)$/)
      mem[r['addr'].to_i] = mask_value(
        r['val'].to_i.to_s(2).rjust(mask.size, '0'),
        mask
      )
    end
  end

  p mem.values.reduce(:+)
end

def part2
  mem = {}
  mask = ''

  File.readlines(DATA_FILE).each do |line|
    if line.start_with? 'mask'
      mask = line.chomp.sub('mask = ', '').split("")
    else
      r = line.match(/^mem\[(?<addr>\d+)\] = (?<val>\d+)$/)
      masked_value = mask_value(
        r['val'].to_i.to_s(2).rjust(mask.size, '0'),
        mask
      )

      addresses = mask_address(
        r['addr'].to_i.to_s(2).rjust(mask.size, '0'),
        mask
      )

      addresses.each do |addr|
        mem[addr] = masked_value
      end
    end
  end

  #p mem.inspect
  p mem.values.reduce(:+)
end

require './runner'
