#!/usr/bin/env ruby

FORMAT =  /(?<lower>\d+)\-(?<upper>\d+) (?<letter>\w): (?<pass>.+?)$/

def part1
  valid = 0
  File.open('data/day2.txt', 'r').each do |line|
    m = line.match(FORMAT)
    count = m[:pass].count(m[:letter])
    if count >= m[:lower].to_i and count <= m[:upper].to_i
      valid +=1
    end
  end
  puts valid
end

def part2
  valid = 0
  File.open('data/day2.txt', 'r').each do |line|
    m = line.match(FORMAT)
    first = m[:pass][m[:lower].to_i - 1]
    second = m[:pass][m[:upper].to_i - 1]

    unless first == m[:letter] and second == m[:letter]
      if first == m[:letter] or second == m[:letter]
        valid += 1
      end
    end
  end
  puts valid
end

require './runner'
