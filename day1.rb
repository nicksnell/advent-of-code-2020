#!/usr/bin/env ruby

def get_lines
  @lines = []
  File.open('data/day1.txt', 'r').each do |line|
    @lines << line.to_i
  end
  return @lines
end

def part1
  @lines = get_lines

  @lines.each_with_index do |v1, i1|
    @lines.each_with_index do |v2, i2|
      if i1 == i2
        next
      end
      if v1 + v2 == 2020
        puts v1 * v2
        return
      end
    end
  end
end

def part2
  @lines = get_lines

  @lines.each_with_index do |v1, i1|
    @lines.each_with_index do |v2, i2|
      if i1 == i2
        next
      end
      @lines.each_with_index do |v3, i3|
        if i1 == i3 or i2 == i3
          next
        end
        if v1 + v2 + v3 == 2020
          puts v1 * v2 * v3
          return
        end
      end
    end
  end
end

require './runner'
