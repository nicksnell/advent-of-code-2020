#!/usr/bin/env ruby

def get_instructions
  instructions = []
  File.readlines(DATA_FILE).each do |line|
    line = line.strip
    instructions << [line[0], line[1..-1].to_i]
  end
  instructions
end

def part1
  headings = ['N', 'E', 'S', 'W']
  face = headings.index('E')
  ship = {'N' => 0, 'S' => 0, 'E' => 0, 'W'=> 0}

  get_instructions.each do |i|
    if i[0] == 'F'
      ship[headings[face]] += i[1]
    elsif headings.include? i[0]
      ship[i[0]] += i[1]
    elsif i[0] == 'L' or i[0] == 'R'
      r = i[1] / 90
      r = r * -1 unless i[0] == 'R'
      headings.rotate!(r)
    end
  end

  puts (
    (ship['N'] - ship['S']).abs +
    (ship['E'] - ship['W']).abs
  )
end

def part2(debug = false)
  headings = ['N', 'E', 'S', 'W']
  ship = {'N' => 0, 'E' => 0, 'S' => 0, 'W'=> 0}
  waypoint = {'N' => 1, 'E' => 10, 'S' => 0, 'W' => 0}

  get_instructions.each do |i|
    if debug
      puts "#{i}"
    end

    if i[0] == 'F'
      n = (waypoint['N'] - waypoint['S']) * i[1]
      e = (waypoint['E'] - waypoint['W']) * i[1]

      if n > 0
        ship['N'] += n
      else
        ship['S'] += n.abs
      end

      if e > 0
        ship['E'] += e
      else
        ship['W'] += e.abs
      end
    elsif headings.include? i[0]
      waypoint[i[0]] += i[1]
    elsif i[0] == 'L' or i[0] == 'R'
      r = i[1] / 90
      r = r * -1 unless i[0] == 'L'
      values = waypoint.values.rotate!(r)
      waypoint = waypoint.keys.zip(values).to_h
    end

    if debug
      puts "Ship: #{ship}"
      puts "Way: #{waypoint}"
      puts
    end
  end

  puts (
    (ship['N'] - ship['S']).abs +
    (ship['E'] - ship['W']).abs
  )
end

require './runner'
