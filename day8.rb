#!/usr/bin/env ruby

def get_ops
  ops = []
  File.open("data/day8.txt", 'r').each do |line|
    m = line.strip!.match(/^(?<op>.+?) \+{0,1}(?<arg>.+?)$/)
    ops << [m[:op], m[:arg].to_i]
  end
  ops
end

def compute(ops, fail_on_loop = false)
  acc = 0
  position = 0
  previous = []

  until position > (ops.size - 1)
    if previous.include? position
      if fail_on_loop
        raise "Looping!"
      end

      break
    end

    cmd = ops[position]
    previous << position

    if cmd[0] == 'acc'
      acc += cmd[1]
    elsif cmd[0] == 'jmp'
      position += cmd[1]
      next
    end

    position += 1
  end

  return {:acc => acc, :previous => previous}
end

def part1
  ops = get_ops
  result = compute ops

  puts "Accumulator: #{result[:acc]}"
  puts "Loop Position: #{result[:previous][-3..-1]}"
end

def part2
  ops = get_ops
  result = nil

  (0..(ops.size - 1)).each do |index|
    test_ops = ops.clone.map(&:clone)

    if test_ops[index][0] == 'acc'
      next
    end

    if test_ops[index][0] == 'jmp'
      test_ops[index][0] = 'nop'
    else
      test_ops[index][0] = 'jmp'
    end

    begin
      result = compute(test_ops, fail_on_loop=true)
    rescue
      next
    else
      break
    end
  end

  puts "Accumulator: #{result[:acc]}"
end

require './runner'
