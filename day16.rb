#!/usr/bin/env ruby

def parse_rule(rule)
  rule.split('-').map(&:to_i)
end

def validate_field(field, rule)
  lower, upper = rule
  if ((field >= lower[0] and field <= lower[1]) or
      (field >= upper[0] and field <= upper[1]))
    return true
  end
end

def validate(ticket, validators)
  ticket.each do |field|
    valid = false

    validators.each do |k, v|
      if validate_field(field, v)
        valid = true
        break
      end
    end

    unless valid
      return field
    end
  end

  return true
end

def process
  validators = {}
  rules, ticket, nearby = File.read(DATA_FILE).split(/\n\n/)

  rules.split("\n").each do |rule|
    m = rule.match(/^(?<name>[a-z\s]+): (?<lhs>\d+-\d+) or (?<rhs>\d+-\d+)$/)
    validators[m['name']] = [
      parse_rule(m['lhs']),
      parse_rule(m['rhs'])
    ]
  end

  ticket = ticket.sub("your ticket:\n", "").strip.split(",").map(&:to_i)

  valid = []
  invalid = []
  nearby = nearby.sub("nearby tickets:\n", "").split("\n")

  nearby.each do |t|
    t = t.split(',').map(&:to_i)
    result = validate(t, validators)
    unless result == true
      invalid << result
    else
      valid << t
    end
  end

  return {
    :ticket => ticket,
    :nearby => nearby,
    :validators => validators,
    :valid => valid,
    :invalid => invalid
  }
end

def part1
  result = process
  p result[:invalid].reduce(:+)
end

def part2
  result = process
  ticket = {}

  result[:ticket].each_with_index do |field, index|
    p "field: #{field} #{index}"
    result[:validators].each do |k, v|
      if ticket.keys.include? k
        next
      end

      if validate_field(field, v)
        # if the field is valid for a rule
        # attempt to prove it matches the nearby
        # tickets, if any don't move on
        nearby = result[:valid].map { |n|
          n[index]
        }.map { |n|
          validate_field(n, v)
        }

        if index == 19
          p nearby.all?
          p k
        end

        if nearby.all?
          ticket[k] = field
          break
        end
      end
    end
  end

  p ticket.keys.size
  p ticket
  p ticket.map { |k, v| v if k.start_with? 'departure' }.compact
end

require './runner'
