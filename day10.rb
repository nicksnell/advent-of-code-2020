#!/usr/bin/env ruby

require 'enumerator'

def get_adapters
  adapters = []
  File.readlines(DATA_FILE).each do |line|
    adapters << line.strip.to_i
  end
  adapters.sort.insert(0, 0)
end

def part1
  diffs = {:ones => 0, :threes => 1}
  get_adapters.each_cons(2) do |last, jolt|
    if jolt - last == 1
      diffs[:ones] += 1
    elsif jolt - last == 3
      diffs[:threes] += 1
    end
  end
  puts diffs[:ones] * diffs[:threes]
end

def search(adapts)
  adapts.each_with_index do |a, i|
    adapts[i+1..-1].each_with_index do |b, j|
      if b - a < 3
        search(adapts[])
      end
    end
  end
end

def part2
  # Refs
  # Combinatorics: https://brilliant.org/wiki/combinatorics/
  # Tribonacci Seq: https://brilliant.org/wiki/tribonacci-sequence/
  diffs = []
  groups = []
  get_adapters.each_cons(2) do |last, jolt|
    if jolt - last == 1
      diffs << 1
    elsif jolt - last == 3
      diffs << 3
    end
  end

  diffs.each do ||
  end

  puts "#{diffs}"
end

require './runner'
