#!/usr/bin/env ruby

def get_bags
  bags = {}
  File.open('data/day7.txt', 'r').each do |line|
    m = line.match(/^(?<color>.+?) bags contain (?<inner>.+?).$/)
    contains = {}
    unless m[:inner] == 'no other bags'
      m[:inner].split(',').each do |constraint|
        inner = constraint.strip.match(/^(?<amount>\d+) (?<color>.+?) (bag|bags)$/)
        contains[inner[:color]] = inner[:amount].to_i
      end
    end
    bags[m[:color]] = contains
  end
  bags
end

def can_contain(bags, color)
  bags.map { |c, cont|
    cont.keys.include?(color) ? c : nil
  }.compact.uniq
end

def get_possible(bags, color)
  colors = can_contain(bags, color)
  others = []

  colors.each do |c|
    others += get_possible(bags, c)
  end

  (colors + others).uniq
end

def get_count(bags, color)
  requires = []
  child_bags = bags[color]

  child_bags.each do |other_color, amount|
    requires << amount
    requires << amount * get_count(bags, other_color)
  end

  requires.sum
end

def part1
  bags = get_bags
  puts "#{get_possible(bags, 'shiny gold').size}"
end

def part2
  bags = get_bags
  puts "#{get_count(bags, 'shiny gold')}"
end

require './runner'
