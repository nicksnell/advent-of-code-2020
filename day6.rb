#!/usr/bin/env ruby

def part1
  yes = 0
  File.read('data/day6.txt').gsub(/(?<=\w)\n/, '').split("\n").each do |line|
    yes += line.split("").uniq.size
  end
  puts yes
end

def part2
  unanimous = 0
  File.read('data/day6.txt').gsub(/(?<=\w)\n/, ' ').split("\n").each do |line|
    people = line.count(' ')
    line.delete(' ').split("").uniq.each do |char|
      if line.count(char) == people
        unanimous += 1
      end
    end
  end
  puts unanimous
end

require './runner'
