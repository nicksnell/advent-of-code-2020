#!/usr/bin/env ruby

def get_row(code)
  rows = (0..127).to_a
  code.split("").each do |weight|
    if weight == 'F'
      rows = (rows[0]..rows[(rows.size / 2).floor]).to_a
    elsif weight == 'B'
      rows = (rows[(rows.size / 2).ceil]..rows[rows.size - 1]).to_a
    end
  end
  rows[0]
end

def get_col(code)
  cols = (0..7).to_a
  code.split("").each do |weight|
    if weight == 'L'
      cols = (cols[0]..cols[(cols.size / 2).floor]).to_a
    elsif weight == 'R'
      cols = (cols[(cols.size / 2).ceil]..cols[cols.size - 1]).to_a
    end
  end
  cols[0]
end

def get_ids
  ids = []
  File.open('data/day5.txt', 'r').each do |line|
    row = get_row line[0..6]
    col = get_col line[7..10]
    ids << row * 8 + col
  end
  ids
end

def part1
  puts get_ids.max
end

def part2
  sorted = get_ids.sort

  (sorted[0]..sorted[-1]).each do |i|
    unless sorted.include? i
      puts i
    end
  end
end

require './runner'
