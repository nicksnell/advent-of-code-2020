#!/usr/bin/env ruby

def get_plan
  plan = []
  File.readlines(DATA_FILE).each do |line|
    plan << line.strip.split("")
  end
  plan
end

def display(plan)
  plan.each do |row|
    puts "#{row.join}"
  end
  puts ""
end

def occupied(plan, x, y)
  occupied = 0
  height = plan.size
  width = plan[0].size

  if x > 0
    above = plan[x - 1]
    (-1..1).each do |z|
      next unless y + z >= 0 and y + z <= width
      if above[y + z] == '#'
        occupied += 1
      end
     end
  end

  if x + 1 < height
    below = plan[x + 1]
    (-1..1).each do |z|
      next unless y + z >= 0 and y + z <= width
      if below[y + z] == '#'
        occupied += 1
      end
    end
  end

  if y - 1 >= 0 and plan[x][y - 1] == '#'
    occupied += 1
  end

  if y + 1 <= width and plan[x][y + 1] == '#'
    occupied += 1
  end

  occupied
end

def sit_down(plan, next_plan)
  plan.each_with_index do |seats, x|
    seats.each_with_index do |seat, y|
      if seat == '.'
        next
      end

      adjacent = occupied(plan, x, y)

      if seat == 'L' and adjacent == 0
        next_plan[x][y] = '#'
      elsif seat == '#' and adjacent >= 4
        next_plan[x][y] = 'L'
      end
    end
  end

  next_plan.clone.map(&:clone)
end

def part1(printer = false)
  plan = get_plan
  next_plan = plan.clone.map(&:clone)

  if printer
    display(plan)
  end

  new_plan = nil

  loop do
    new_plan = sit_down(plan, next_plan)

    if printer
      display(new_plan)
    end

    break unless new_plan.difference(plan).any?

    plan = new_plan
    next_plan = plan.clone.map(&:clone)
  end

  puts new_plan.flatten(1).count{ |i| i == '#' }
end

def occupied_visual(plan, x, y)
  occupied = 0
  height = plan.size
  width = plan[0].size

  # Above
  if x - 1 >= 0
    # Up
    (1..x).each do |z|
      eyeline = plan[x - z][y]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
    # Up right
    (1..x).each do |z|
      break if y + z < 0
      eyeline = plan[x - z][y + z]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
    # Up left
    (1..x).each do |z|
      break if y - z < 0
      eyeline = plan[x - z][y - z]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
  end

  # Below
  if x + 1 <= height
    # Down
    (x + 1...height).each do |z|
      eyeline = plan[z][y]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
    # Down right
    (x + 1...height).each do |z|
      break unless y + z - x < width
      eyeline = plan[z][y + (z - x)]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
    # Down left
    (x + 1...height).each do |z|
      break unless y - (z - x) >= 0
      eyeline = plan[z][y - (z - x)]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
  end

  # Left
  if y - 1 >= 0
    (1..y).each do |z|
      eyeline = plan[x][y - z]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
  end

  # Right
  if y + 1 <= width
    (y + 1...width).each do |z|
      eyeline = plan[x][z]
      break if eyeline == 'L'
      if eyeline == '#'
        occupied += 1
        break
      end
    end
  end

  occupied
end

def sit_down_actual(plan, next_plan)
  plan.each_with_index do |seats, x|
    seats.each_with_index do |seat, y|
      if seat == '.'
        next
      end

      line_of_sight = occupied_visual(plan, x, y)

      if seat == 'L' and line_of_sight == 0
        next_plan[x][y] = '#'
      elsif seat == '#' and line_of_sight >= 5
        next_plan[x][y] = 'L'
      end
    end
  end

  next_plan.clone.map(&:clone)
end

def part2(printer = false)
  plan = get_plan
  next_plan = plan.clone.map(&:clone)

  if printer
    display(plan)
  end

  new_plan = nil

  loop do
    new_plan = sit_down_actual(plan, next_plan)

    if printer
      display(new_plan)
    end

    break unless new_plan.difference(plan).any?

    plan = new_plan
    next_plan = plan.clone.map(&:clone)
  end

  puts new_plan.flatten(1).count{ |i| i == '#' }
end

require './runner'
