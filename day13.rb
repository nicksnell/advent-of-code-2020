#!/usr/bin/env ruby

def part1
  depart, buses = File.read(DATA_FILE).lines
  earliest = {}
  depart = depart.to_i

  buses.strip.split(',').select { |i| i != 'x' }.map(&:to_i).each do |id|
    diff = depart % id
    earliest[id] = id - diff + depart
  end

  id, time = earliest.min_by { |k, v| v }
  puts id * (time - depart)
end

def part2
  buses = File.read(DATA_FILE).lines[1].strip.split(',').map.with_index { |id, index|
    next if id == 'x'
    id = id.to_i
    [id, (id - index) % id]
  }.compact

  offset, time = buses.first
  i = 0

  while i < buses.size - 1
    nxt_bus, nxt_bus_wait = buses[i + 1]
    until time % nxt_bus == nxt_bus_wait
      time += offset
    end
    offset = offset * nxt_bus
    i += 1
  end

  puts time
end

require './runner'
